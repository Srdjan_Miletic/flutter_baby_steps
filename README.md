# my_first_flutter_app

Messing around with flutter a little bit. Initially based on the flutter tutorial but
then I started to mess around a bit with redux and making stuff more FP ish.

The purpoes of this mini-project is more to learn flutter than to actually build anything
useful so, you know, expect clean code but a hot gabage app no sane user would want.

# Code Style
- Use dartfmt or your IDE's auto format
- Don't hardcode anything, including routes.
- You should almost never use stateful components. State is icky
- Ditto for mutability
