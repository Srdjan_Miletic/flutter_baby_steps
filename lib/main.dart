import 'package:flutter/material.dart';
import 'package:my_first_flutter_app/routes/Routes.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:my_first_flutter_app/store/AppState.dart';
import 'package:my_first_flutter_app/reducers/AppReducer.dart';

void main() => runApp(new StartupNameListApp());

class StartupNameListApp extends StatelessWidget {
  final store = new Store<AppState>(
    appReducer,
    initialState: new AppState([]),
    middleware: [],
  );

  @override
  Widget build(BuildContext context) => StoreProvider(
      store: this.store,
      child: new MaterialApp(
          title: 'Startup Name Generator',
          initialRoute: 'home',
          routes: routesMap));
}
