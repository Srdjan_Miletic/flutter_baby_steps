import 'package:flutter/material.dart';
import 'package:my_first_flutter_app/components/Home.dart';
import 'package:my_first_flutter_app/components/RandomWordsInfiniteList.dart';

final Map<String, Widget Function(BuildContext)> routesMap = {
  '/': (_) => Home(), // Dirty hack so we can use an enum for our routes
  routes.home.toString(): (_) => Home(),
  routes.randomWordList.toString(): (_) => RandomWordsInfiniteList(),
};

enum routes {
  home,
  randomWordList,
}