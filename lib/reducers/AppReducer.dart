import 'package:english_words/english_words.dart';
import 'package:my_first_flutter_app/store/AppState.dart';
import 'package:my_first_flutter_app/actions/Actions.dart';

AppState appReducer(AppState state, dynamic action) {
  switch (action) {
    case Actions.generateMoreNames:
      final wordpairsToAdd = generateWordPairs().take(5).toList();
      return new AppState(state.startupNames + wordpairsToAdd);
    default:
      return state;
  }
}
