import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:my_first_flutter_app/store/AppState.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:my_first_flutter_app/actions/Actions.dart';

class RandomWordsInfiniteList extends StatelessWidget {
  final AppBar _startupNameGenTitleBar =
      new AppBar(title: new Text('Startup Name Generator'));

  @override
  Widget build(BuildContext context) => new Scaffold(
        appBar: _startupNameGenTitleBar,
        body: _listWithPadding(),
        floatingActionButton: floatingGenerateItemsButton(),
      );

  Widget _listWithPadding() => new StoreConnector<AppState, List<WordPair>>(
      converter: (store) => store.state.startupNames,
      builder: (context, names) => new ListView.builder(
          padding: const EdgeInsets.all(16.0),
          itemCount: names.length * 2,
          itemBuilder: (BuildContext _context, int i) =>
              i.isOdd ? new Divider() : _listItem(names[(i / 2).round()])));

  Widget _listItem(WordPair pair) => new ListTile(
        title: new Text(
          pair.asPascalCase,
          style: const TextStyle(fontSize: 18.0),
        ),
      );

  Widget floatingGenerateItemsButton() =>
      new StoreConnector<AppState, VoidCallback>(
          converter: (store) => () => store.dispatch(Actions.generateMoreNames),
          builder: (context, callback) => new FloatingActionButton(
                onPressed: callback,
                child: Icon(Icons.add),
              ));
}
