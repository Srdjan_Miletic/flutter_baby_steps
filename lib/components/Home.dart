import 'package:flutter/material.dart';

import 'package:my_first_flutter_app/routes/Routes.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) => new Scaffold(
      appBar: new AppBar(title: new Text('Choose')),
      body: new Center(
        child: new RaisedButton(
            onPressed: () =>
                Navigator.pushNamed(context, routes.randomWordList.toString()),
            child: Icon(Icons.arrow_forward)),
      ));
}
